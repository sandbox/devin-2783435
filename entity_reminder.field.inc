<?php

/**
 * Implements hook_field_info().
 */
function entity_reminder_field_info() {
  return array(
    'entity_reminder' => array(
      'label' => t('Entity reminder'),
      'description' => t('Add entity reminders.'),
      'default_widget' => 'entity_reminder',
      'default_formatter' => 'entity_reminder_default',
    ),
  );
}

/**
 * Implements hook_element_info().
 */
function entity_reminder_element_info() {
  $elements = array();
  $elements['entity_reminder'] = array(
    '#input' => TRUE,
    '#process' => array('entity_reminder_field_process'),
    //'#theme' => 'entity_reminder',
    '#theme_wrappers' => array('form_element'),
  );
  return $elements;
}

/**
 * Implements hook_field_formatter_info().
 */
function entity_reminder_field_formatter_info() {
  return array(
    'entity_reminder_default' => array(
      'label' => t('Default'),
      'field types' => array('entity_reminder'),
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function entity_reminder_field_is_empty($item, $field) {
  // Assume the item is "empty" only if the message field is missing.
  if (empty($item['message']['value'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_widget_info().
 *
 * Here we indicate that the content module will handle the multiple values for
 * this widget, but that the default value callback is custom.
 *
 * @see hook_widget().
 */
function entity_reminder_field_widget_info() {
  return array(
    'entity_reminder' => array(
      'label' => t('Entity reminder'),
      'field types' => array('entity_reminder'),
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'description' => t('A widget for entity reminders.'),
      'settings' => array(),
    ),
  );
}

/**
 * Implements hook_widget().
 *
 * Return a skeleton FAPI array that defines callbacks for the widget form.
 */
function entity_reminder_field_process($element, $form_state, $complete_form) {
  $reminder_form = & $element;
  $element['#tree'] = TRUE;

  $reminder_form['rid'] = array(
    '#type' => 'hidden',
  );

  $reminder_form['nothing']['#markup'] = '<div class="container-inline">';
  $reminder_form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
  );

  $reminder_form['nothing2']['#markup'] = '</div>';

  // Start inline elements.
  $reminder_form['nothing3']['#markup'] = '<div class="container-inline">';

  // @todo make configurable
  $units = array(
    '86400' => array('max' => 30, 'step size' => 1),
    '3600' => array('max' => 24, 'step size' => 1),
    '60' => array('max' => 60, 'step size' => 1),
  );

  $reminder_form['offset'] = array(
    '#title' => t('Send reminder'),
    '#prefix' => '<div class="form-item"><div id="' . $element['#id'] . '-offset">',
    '#suffix' => '</div></div>',
    '#type' => 'timeperiod_select',
    '#units' => $units,
    '#states' => array(
      'disabled' => array(
        "#{$element['#id']}-type" => array('value' => 'date'),
      ),
    ),
  );

  $reminder_form['direction'] = array(
    '#type' => 'select',
    '#options' => array(
      'before' => t('Before'),
      'after' => t('After'),
    ),
  );

  ctools_include('plugins');
  $event_plugins = ctools_get_plugins('entity_reminder', 'reminder_event');
  foreach ($event_plugins as $plugin_key => $plugin) {
    $class = ctools_plugin_get_class($plugin, 'handler');
    $eventPlugin = new $class();
    if ($events = $eventPlugin->getEvents($element['#entity_type'], $element['#entity'])) {
      foreach ($events as $ekey => $event) {
        $star = !empty($plugin['provides']) ? '*' : '';
        $values[$plugin_key . ($ekey ? '-' . $ekey : '')] = $plugin['title'] . $star . ($event ? ': ' . $event : '');
      }
    }
  }

  $reminder_form['type'] = array(
    //'#title' => t('Event'),
    '#type' => 'select',
    '#options' => $values,
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'eventoptioncallback',
      'method' => 'replace',
      'wrapper' => "{$element['#id']}-type-options",
    ),
  );


  $reminder_form['type_options']['#prefix'] = '<div id="' . "{$element['#id']}-type-options" . '">';
  $reminder_form['type_options']['#suffix'] = '</div></div>';

  $reminder_form['intersect'] = array(
    '#title' => t('Intersect'),
    '#description' => t('The eligible users from the event will be intersected with the reminder list. This allows for more advanced configurations.'),
    '#type' => 'checkbox',
  );

  $reminder_form['repeat'] = array(
    '#title' => t('Repeat reminder'),
    '#type' => 'checkbox',
  );

  ctools_include('plugins');
  $plugins = ctools_get_plugins('entity_reminder', 'reminder_recipients');
  foreach ($plugins as $key => $plugin) {
    $class = ctools_plugin_get_class($plugin, 'handler');
    $instance = new $class();
    if ($lists = $instance->getLists($element['#entity_type'], $element['#entity'])) {
      foreach ($lists as $lkey => $list) {
        $sendto[$key . ($lkey ? '-' . $lkey : '')] = $plugin['title'] . ($list ? ': ' . $list : '');
      }
    }
  }

  $reminder_form['every'] = array(
    '#prefix' => '<div class="form-item"><div id="' . $element['#id'] . '-every">',
    '#suffix' => '</div></div>',
    '#title' => 'Every',
    '#type' => 'timeperiod_select',
    '#units' => $units,
    '#states' => array(
      'visible' => array(
        "#{$element['#id']}-repeat" => array('checked' => TRUE),
      ),
    ),
  );

  $reminder_form['for'] = array(
    '#prefix' => '<div class="form-item"><div id="' . $element['#id'] . '-for">',
    '#suffix' => '</div></div>',
    '#title' => 'For',
    '#type' => 'timeperiod_select',
    '#units' => $units,
    '#states' => array(
      'visible' => array(
        "#{$element['#id']}-repeat" => array('checked' => TRUE),
      ),
    ),
  );

  if (module_exists('date_popup')) {
    // Display conditionally.
    $reminder_form['date'] = array(
      '#title' => t('Please specify a date'),
      '#type' => 'date_popup',
      '#size' => 20,
      '#states' => array(
        'visible' => array(
          "#{$element['#id']}-type" => array('value' => 'date-date'),
        ),
      ),
    );

    $reminder_form['date']['#type'] = 'date_popup';
    $reminder_form['date']['#date_format'] = 'm/d/Y g:ia';
    $reminder_form['date']['#date_year_range'] = '0:+3';
  }

  $reminder_form['to'] = array(
    '#title' => t('Recipients'),
    '#type' => 'select',
    '#options' => $sendto,
  );


  $reminder_form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => t('Reminder from [site:name]'),
  );

  $reminder_form['message'] = array(
    '#type' => 'text_format',
    '#title' => t('Message'),
    '#wysiwyg' => TRUE,
  );

  // Set a flag so we know this is a user submission, and not a programmatic
  // save.
  $reminder_form['prepare'] = array(
    '#type' => 'value',
    '#value' => 1,
  );

  $reminder_form['#element_validate'] = array('entity_reminder_element_validate');


  $reminder_form['token_container'] = array(
    '#type' => 'fieldset',
    '#title' => 'Replacement tokens',
  );

  $reminder_form['token_container']['tokens'] = array(
    '#markup' => theme('token_tree', array('dialog' => TRUE, 'token_types' => array('signup', 'entity', 'global', 'user'))),
  );

  if (!$form_state['process_input']) {
    if (!empty($reminder_form['#value'])) {
      foreach ($reminder_form['#value'] as $plugin_key => $value) {
        if ($plugin_key == 'message' && is_array($value)) {
          $reminder_form['message']['#format'] = $value['format'];
          $reminder_form['message']['#default_value'] = $value['value'];
        }
        elseif ($plugin_key == 'date' && module_exists('date_popup') && !empty($value)) {
          // Date_popup won't accept a timestamp, so convert to string
          // And date_popup requires the default_value to be in Y-m-d format
          // Regardless of the actual configured format (how it gets output)
          $reminder_form[$plugin_key]['#default_value'] = date('Y-m-d', $value);
        }
        else {
          $reminder_form[$plugin_key]['#default_value'] = $value;
        }

        if ($plugin_key == 'message_format') {
          $reminder_form['message']['#format'] = $value;
        }
      }
    }
  }

  $reminder_form['send_test'] = array(
    '#title' => 'Send test',
    '#type' => 'checkbox',
  );

  return $reminder_form;
}

function eventoptioncallback($form, $form_state, $df) {
  $event = $form_state['triggering_element']['#value'];

  $plugin = explode('-', $event);
  $cp = ctools_get_plugins('entity_reminder', 'reminder_event', $plugin[0]);
  $class = ctools_plugin_get_class($cp, 'handler');
  $eventPlugin = new $class();
  $parents = $form_state['triggering_element']['#array_parents'];
  array_pop($parents);
  $val = &drupal_array_get_nested_value($form, $parents);
  $val['type_options'] = $eventPlugin->buildForm($plugin[1], $form['#entity'], $form['#entity_type']);
  $val['type_options']['#prefix'] = '<div id="' . "{$val['#id']}-type-options" . '">';
  $val['type_options']['#suffix'] = '</div>';
  return $val['type_options'];
}

function entity_reminder_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] == 'entity_reminder') {
    foreach ($items as $delta => &$item) {

      // Generate a new reminder ID if this is a new entity or no reminder ID
      // exists.
      $item['rid'] = (!isset($entity->nid) || empty($item['rid'])) ? uniqid() : $item['rid'];

      if (!is_numeric($item['date'])) {
        if (!strtotime($item['date'])) {
          // Unset date if it is not a timestamp and not a date string.
          unset($item['date']);
        }
        else {
          // Otherwise, convert to a timestamp.
          $item['date'] = strtotime($item['date']);
        }
      }

      if (is_array($item['message'])) {
        // Extract message if it is an array.
        $message_value = $item['message']['value'];
        $message_format = $item['message']['format'];
        unset($item['message']);
        $item['message'] = $message_value;
        $item['message_format'] = $message_format;
      }

      if ($item['send_test']) {
        // Send test if requested.
        global $user;
        $params = array(
          'user' => $user,
          'message' => check_markup($message_value, $message_format),
          'entity' => $entity,
          'reminder' => $item,
        );

        drupal_mail('entity_reminder', 'reminder', $user->mail, user_preferred_language($user), $params);
      }
    }
  }
}

/**
 * Process an individual element.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function entity_reminder_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element += array(
    '#type' => $instance['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  return $element;
}
