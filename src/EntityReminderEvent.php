<?php

/**
 * Base class for an Entity reminder event.
 */
abstract class EntityReminderEvent {

  /**
   * Get a list of events for this entity.
   *
   * An event is anything that provides a date. For example:
   *  - a node being created
   *  - a specific time passing
   *
   * Some events can return users. For example:
   *  - a user visiting the node
   *
   * These dates have users as well!
   *
   * @param type $entity
   * @param type $entity_type
   *
   * @return array
   *   An array of events, keyed by event key.
   */
  abstract function getEvents($entity_type, $entity);

  /**
   * Some event dates have users. This should return the users that this event
   * may send to. For example, if this was for "a user visiting the node", this
   * method should return an array of users and the date that they visited.
   *
   * @param string $op
   * @param string $entity_type
   * @param array $ids
   *
   * @return array
   *   An array keyed by user ID, containing an array keyed by:
   *   - uid: User ID of the actionable user
   *   - date: The date of the action
   */
  function getEventActors($op, $entity_type, $ids) {
    return array();
  }

  function buildForm($entity_type, $entity) {
    return array();
  }
}
