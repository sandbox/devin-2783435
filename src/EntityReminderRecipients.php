<?php

/**
 * Base class for reminder recipients.
 */
abstract class EntityReminderRecipients {

  /**
   * Return an array of lists.
   *
   * @param type $entity_type
   * @param type $entity
   *
   * @return array
   *   An array of list names keyed by the list type.
   */
  abstract function getLists($entity_type, $entity);

  /**
   * Get eligible users to send mail to. Method should return an array of
   * eligible users keyed by uid with a timestamp value.
   *
   * If no timestamp is available (for example, all users in a role) then just
   * keying by uid and value of uid is fine.
   *
   * @param type $event_key
   *   Event key from getLists().
   * @param array $context
   *   Context used to make a recipient list. Typically keyed by the entity
   *   type, containing the entity to use in making the recipient list.
   *
   * @return array
   *   An array keyed by user ID containing an element "uid".
   *
   */
  function getRecipients($op, array $context) {

  }

}
