<?php

$plugin = array(
  'title' => t('Date'),
  'description' => t('Controls access by conditional dates'),
  'handler' => array(
    'class' => 'EntityReminderEventDates',
  ),
  // Does not provide anything. Global event.
  'provides' => array(),
);

class EntityReminderEventDates extends EntityReminderEvent {

  /**
   * This is a static date that applies to all recipients.
   */
  function getEvents($entity_type, $entity = NULL) {
    if ($entity) {
      // We need an entity to get the date fields on it.
      list($entity_id,, $bundle) = entity_extract_ids($entity_type, $entity);
      // Support all configurable date elements currently set on this entity.
      $dates = array();
      $fields = field_info_instances($entity_type, $bundle);
      foreach ($fields as $bundle_field) {
        $field_info = field_info_field($bundle_field['field_name']);
        if ($field_info['type'] == 'datetime') {
          $dates["{$bundle_field['field_name']}|value"] = $bundle_field['label'] . ' ' . t('start');
          $dates["{$bundle_field['field_name']}|value2"] = $bundle_field['label'] . ' ' . t('end');
        }
      }

      return $dates;
    }
  }

}
