<?php

$plugin = array(
  'title' => t('Date'),
  'description' => t('Provides reminder events based on an arbitrary date.'),
  'handler' => array(
    'class' => 'EntityReminderSpecificDate',
  ),
  // Does not provide anything. Global event.
  'provides' => array(),
);

class EntityReminderSpecificDate extends EntityReminderEvent {

  public function getEvents($entity_type, $entity) {
    return array(
      'date' => t('Specific date'),
    );
  }

  public function buildForm($entity_type, $entity) {
    if (module_exists('date_popup')) {
      $reminder_form['date'] = array(
        '#title' => t('Please specify a date'),
        '#type' => 'date_popup',
      );

      $reminder_form['date']['#date_format'] = 'm/d/Y g:ia';
      $reminder_form['date']['#date_year_range'] = '0:+3';
      return $reminder_form;
    }
  }

}
