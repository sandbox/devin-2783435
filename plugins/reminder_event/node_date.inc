<?php

$plugin = array(
  'title' => t('Node date'),
  'description' => t('Provides reminder events based on node dates'),
  'handler' => array(
    'class' => 'EntityReminderEventNodeDate',
  ),
  // Does not provide anything. Global event.
  'provides' => array(),
);

class EntityReminderEventNodeDate extends EntityReminderEvent {

  /**
   * This is a static date that applies to all recipients.
   */
  function getEvents($entity, $entity_type) {
    if ($entity_type == 'node') {
      $dates = array();

      $dates['node_created'] = t('Created');
      $dates['node_changed'] = t('Changed');

      return $dates;
    }
  }

}
