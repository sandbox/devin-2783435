<?php

$plugin = array(
  'title' => t('Node author'),
  'description' => t('Node author'),
  'handler' => array(
    'class' => 'EntityReminderRecipientsNodeAuthor',
  ),
  'requires' => array(
    'node',
  ),
);

class EntityReminderRecipientsNodeAuthor extends EntityReminderRecipients {

  function getLists($entity_type, $entity) {
    return array(
      'user' => t('User'),
    );
  }

  /**
   * Send to node author.
   */
  public function getRecipients($op, array $context) {
    if ($context['node']) {
      $uid = $context['node']->uid;
      return array(
        $uid => array('uid' => $context['node']->uid),
      );
    }
  }

}
