<?php

$plugin = array(
  'title' => t('Role'),
  'description' => t('Users in a role.'),
  'handler' => array(
    'class' => 'RoleReminderList',
  ),
);

class RoleReminderList extends EntityReminderRecipients {

  function getLists($entity_type, $entity) {
    return user_roles();
  }

}
