<?php

$plugin = array(
  'title' => t('Specific user'),
  'description' => t('A specific user.'),
  'handler' => array(
    'class' => 'EntityReminderRecipientsSpecificUser',
  ),
);

class EntityReminderRecipientsSpecificUser extends EntityReminderRecipients {

  function getLists($entity_type, $entity) {
    return array('user' => t('User'));
  }

  /**
   * Send to a specific user.
   */
  public function getRecipients($op, array $context) {
    if ($context['user']) {
      return user_load($context['user'])->mail;
    }
  }

  public function form() {
    $config['mail'] = array(
      '#title' => 'Mail',
      '#type' => 'textfield',
    );
    return $config;
  }

  public function form_submit() {

  }

  public function form_validate() {

  }

}
