<?php

$plugin = array(
  'title' => t('User'),
  'description' => t('Plugin-provided user.'),
  'handler' => array(
    'class' => 'EntityReminderRecipientsUser',
  ),
  'needs' => array(
    'user',
  ),
);

/**
 * Send to a user provided by the context.
 */
class EntityReminderRecipientsUser extends EntityReminderRecipients {

  function getLists($entity_type, $entity) {
    return array('user' => t('User from event'));
  }

  /**
   * Send to the context user.
   */
  public function getRecipients($op, array $context) {
    // Stub, never called.
  }

}
